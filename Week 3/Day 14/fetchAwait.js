const fetch = require("node-fetch");

const fetchApi = async () => {
  let urlCatFact = "https://catfact.ninja/fact";
  let urlRandomUser = "https://randomuser.me/api/";
  let urlImageDog = "https://dog.ceo/api/breeds/image/random";
  let data = {};

  try {
    const responseCatFact = await fetch(urlCatFact);
    const responseRandomUser = await fetch(urlRandomUser);
    const responseImageDog = await fetch(urlImageDog);

    // data = {
    //   catFact: await responseCatFact.json(),
    //   randomUser: await responseRandomUser.json(),
    //   imageDog: await responseImageDog.json(),
    // };

    const response = await Promise.all([
      responseCatFact.json(),
      responseRandomUser.json(),
      responseImageDog.json(),
    ]);

    data = {
      catFact: response[0],
      randomUser: response[1],
      imageDog: response[2],
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
