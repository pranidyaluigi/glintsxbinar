const axios = require("axios");

const fetchApi = async () => {
  let urlCatFact = "https://catfact.ninja/fact";
  let urlRandomUser = "https://randomuser.me/api/";
  let urlImageDog = "https://dog.ceo/api/breeds/image/random";
  let data = {};

  try {
    // const responseCatFact = await axios.get(urlCatFact);
    // const responseRandomUser = await axios.get(urlRandomUser);
    // const responseImageDog = await axios.get(urlImageDog);

    const response = await Promise.all([
      axios.get(urlCatFact),
      axios.get(urlRandomUser),
      axios.get(urlImageDog),
    ]);

    data = {
      catFact: response[0].data,
      randomUser: response[1].data.results,
      imageDog: response[2].data,
    };

    console.log(data);
  } catch (error) {
    console.error(error.message);
  }
};

fetchApi();
