const axios = require("axios");
const { promises } = require("stream");

let urlCatFact = "https://catfact.ninja/fact";
let urlRandomUser = "https://randomuser.me/api/";
let urlImageDog = "https://dog.ceo/api/breeds/image/random";
let data = {};

axios
  .get(urlCatFact)

  .then((response) => {
    data = { catFact: response.data };

    return axios.get(urlRandomUser);
  })

  .then((response) => {
    data = { ...data, randomUser: response.data.results };

    return axios.get(urlImageDog);
  })
  .then((response) => {
    data = { ...data, albums: response.data };

    console.log(data);
  })
  .catch((err) => {
    console.log(err.message);
  });
