const ThreeDimention = require();

class Pyramid extends ThreeDimention{
    constructor(base, height) {
        super('Pyramid');
        this.base = base;
        this.height = height;
        this.heightTriangle = this.heightTriangle;
    }
  
    //Overiding method
    calculateVolume(){
        super.calculateVolume();
        return (this.base * 2) * this.height / 3;
    }

    heightTriangle = function () {
        return Math.sqrt((this.base ** 2) + (this.height **2));
    }

    calculateCircumeference() {
        super.calculateCircumeference();
        return (this.base * 4) + (this.heightTriangle * this.base * 2);
    }
}

module.exports = Pyramid;