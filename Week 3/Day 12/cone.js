class Cone extends ThreeDimention {
  constructor(radius, height) {
    super("Cone");
    this.radius = radius;
    this.height = height;
    this.linePainter = linePainter();
  }

  calculateVolume() {
    super.calculateArea();
    return Math.PI * this.radius ** 3;
  }

  linePainter() {
    return Math.sqrt(this.radius ** 2 + this.height ** 2);
  }

  calculateSurfaceArea() {
    super.calculateSurfaceArea();
    return Math.PI * this.radius * (this.radius + this.linePainter());
  }
}
