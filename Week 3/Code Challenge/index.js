const express = require("express");
const colour = require("./routes/colour");

const app = express();

const port = process.env.PORT || 3000;

app.use(express.json()); // enable read req.body (JSON)
// Enable req.body (URL-Encoded)
// app.use(
//   express.urlencoded({
//     extended: true,
//   })
// );

app.use("/colour", colour);

app.listen(port, () => {
  console.log(`Server running at port ${port}`);
});
