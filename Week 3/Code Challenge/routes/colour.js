const express = require("express");

const {
  getAllColour,
  getDetailColour,
  addColour,
  updateColour,
  deleteColour,
} = require("../controllers/colour");

const router = express.Router();

router.get("/", getAllColour);
router.get("/:id", getDetailColour);
router.post("/", addColour);
router.put("/:id", updateColour);
router.delete("/:id", deleteColour);

module.exports = router;
