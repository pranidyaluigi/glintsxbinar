let data = require("../models/data.json");

class Colour {
  getAllColour(req, res, next) {
    try {
      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  getDetailColour(req, res, next) {
    try {
      const detailData = data.filter(
        (item) => item.id === parseInt(req.params.id)
      );

      if (detailData.length === 0) {
        return res.status(404).json({ errors: ["Colours are not found"] });
      }

      res.status(200).json({ data: detailData });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  addColour(req, res, next) {
    try {
      let lastId = data[data.length - 1].id;

      data.push({
        id: lastId + 1,
        colour: req.body.colour,
        characters: req.body.characters,
      });

      res.status(201).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  updateColour(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      if (!findData) {
        return res.status(404).json({ errors: ["Colours are not found"] });
      }

      data = data.map((item) =>
        item.id === parseInt(req.params.id)
          ? {
              id: parseInt(req.params.id),
              colour: req.body.colour,
              characters: req.body.characters,
            }
          : item
      );

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }

  deleteColour(req, res, next) {
    try {
      let findData = data.some((item) => item.id === parseInt(req.params.id));

      if (!findData) {
        return res.status(404).json({ errors: ["Colours are not found"] });
      }

      data = data.filter((item) => item.id !== parseInt(req.params.id));

      res.status(200).json({ data: data });
    } catch (error) {
      res.status(500).json({
        errors: ["Internal Server Error"],
      });
    }
  }
}

module.exports = new Colour();
