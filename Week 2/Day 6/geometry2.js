const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function squareLimas(width, lowerHeight, height) {
  return width * lowerHeight * height;
}

function inputWidth() {
  rl.question(`Please, input the width of Square Limas : `, (width) => {
    if (!isNaN(width)) {
      inputLowerHeight(width);
    } else {
      console.log(`Sorry, the data you provided was misplaced\n`);
      inputWidth();
    }
  });
}

function inputLowerHeight(width) {
  rl.question(
    `Please, input the lower height of Square Limas : `,
    (lowerHeight) => {
      if (!isNaN(lowerHeight)) {
        inputHeight(width, lowerHeight);
      } else {
        console.log(`Sorry, the data you provided was misplaced\n`);
        inputLowerLength(width);
      }
    }
  );
}

function inputHeight(lowerHeight, width) {
  rl.question(`Please, input the height of Square Limas : `, (height) => {
    if (!isNaN(height)) {
      const result = `/nSquare Limas's Volume : ${squareLimas(
        width,
        lowerHeight,
        height
      )}`;
      console.log(result);
      rl.close();
    } else {
      console.log(`Sorry, the data you provided was misplaced\n`);
      inputHeight(width, lowerHeight);
    }
  });
}

console.log(`Square Limas`);
console.log(`=========`);
inputWidth();
