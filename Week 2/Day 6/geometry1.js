const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

function triangleLimas(pedestal, lowerHeight, height) {
  return ((pedestal * lowerHeight) / 2) * height;
}

function inputPedestal() {
  rl.question(`Please, input the pedestal of Triangle Limas : `, (pedestal) => {
    if (!isNaN(pedestal)) {
      inputLowerHeight(pedestal);
    } else {
      console.log(`Sorry, the data you provided was misplaced\n`);
      inputPedestal();
    }
  });
}

function inputLowerHeight(pedestal) {
  rl.question(
    `Please, input the lower height of Triangle Limas : `,
    (lowerHeight) => {
      if (!isNaN(lowerHeight)) {
        inputHeight(pedestal, lowerHeight);
      } else {
        console.log(`Sorry, the data you provided was misplaced\n`);
        inputLowerLength(pedestal);
      }
    }
  );
}

function inputHeight(lowerHeight, pedestal) {
  rl.question(`Please, input the height of Triangle Limas : `, (height) => {
    if (!isNaN(height)) {
      const result = `/nTriangle Limas's Volume : ${triangleLimas(
        pedestal,
        lowerHeight,
        height
      )}`;
      console.log(result);
      rl.close();
    } else {
      console.log(`Sorry, the data you provided was misplaced\n`);
      inputHeight(pedestal, lowerHeight);
    }
  });
}

console.log(`Triangle Limas`);
console.log(`=========`);
inputPedestal();
