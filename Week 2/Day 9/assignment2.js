let people = [
  {
    name: "John",
    status: "Positive",
  },
  {
    name: "Maria",
    status: "Positive",
  },
  {
    name: "Paula",
    status: "Positive",
  },
  {
    name: "Mike",
    status: "Suspect",
  },
  {
    name: "Mario",
    status: "Suspect",
  },
  {
    name: "Charlie",
    status: "Suspect",
  },
  {
    name: "Clara",
    status: "Negative",
  },
  {
    name: "Nara",
    status: "Negative",
  },
  {
    name: "Bruno",
    status: "Negative",
  },
];

const positivePeople = [];
const negativePeople = [];
const suspectPeople = [];

let positiveStatus = people
  .filter((person) => person.status === "Positive")
  .map((detail) => detail.name);

let negativeStatus = people
  .filter((person) => person.status === "Negative")
  .map((detail) => detail.name);

let suspectStatus = people
  .filter((person) => person.status === "Suspect")
  .map((detail) => detail.name);

let userChoice = 1;
switch (userChoice) {
  case 1:
    console.log(`${positiveStatus} is Positive`);
    break;
  case 2:
    console.log(`${negativeStatus} is Negative`);
    break;
  default:
    console.log(`${suspectStatus} is Suspect`);
}
let listOfPatients = [userChoice];
module.exports.listOfPatients = listOfPatients;

// function pasien(hasilTest) {
//   let i = 0;
//   do {
//     if (people[i].status == hasilTest) {
//       console.log(`${hasilTest} : ${people[i].name}`);
//     }
//     i++;
//   } while (i < people.length);
// }
// pasien("Positive");
// pasien("Negative");
// pasien("Suspect");

// console.log(listOfPatients);
