let vegetables = ["Tomato", "Broccoli", "Kale", "Cabbage", "Apple"];
let justVegetables = vegetables.pop();

for (let i = 0; i < vegetables.length; i++) {
  let vegetable = vegetables[i];
  console.log(`${vegetable} is a healthy food, it's definitely worth to eat.`);
}
