const data = require("./lib/arrayFactory.js");
const test = require("./lib/test.js");

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter((x) => x);
}

// Should return array
function sortAscending(data) {
  // Code Here
  for (let i = 0; i < data.length; i++) {
    for (let j = 1; j < i; j++) {
      if (data[i] < data[j]) {
        let dataAscending = data[i];
        data[i] = data[j];
        data[j] = dataAscending;
      }
    }
  }
  return data.filter((x) => x);
}

// Should return array
function sortDecending(data) {
  // Code Here
  for (let i = 0; i < data.length; i++) {
    for (let j = 1; j < i; j++) {
      if (data[i] > data[j]) {
        let dataAscending = data[i];
        data[i] = data[j];
        data[j] = dataAscending;
      }
    }
  }
  return data.filter((x) => x);
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);
