/*
 * Below is an operation of triangleLimas Volume
 */

function triangleLimas(pedestal, lowerHeight, height) {
  console.log("return: " + ((pedestal * lowerHeight) / 2) * height);
  return ((pedestal * lowerHeight) / 2) * height;
}

let volumeTriangleLimasOne = triangleLimas(2, 4, 6);
let volumeTriangleLimasTwo = triangleLimas(5, 6, 10);
console.log(
  "volumeTriangleLimasOne + volumeTriangleLimasTwo : " +
    (volumeTriangleLimasOne + volumeTriangleLimasTwo)
);

/*
 * Below is an operation of squareLimas Volume
 */

function squareLimas(width, lowerHeight, height) {
  console.log("return: " + width * lowerHeight * height);
  return width * lowerHeight * height;
}

let volumeSquareLimasOne = squareLimas(3, 4, 6);
let volumeSquareLimasTwo = squareLimas(7, 5, 11);
console.log(
  "volumeSquareLimasOne + volumeSquareLimasTwo : " +
    (volumeSquareLimasOne + volumeSquareLimasTwo)
);
